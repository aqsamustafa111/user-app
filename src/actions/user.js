import axios from 'axios';
import { BASE_URL } from '../config';

const user = {
  fetchUsers: () => {
    return axios
      .get(`${BASE_URL}/users`)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        if (error.response) {
          return error.response;
        }
        return 'An error occurred';
      });
  },
};

export default user;
