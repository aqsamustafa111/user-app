import React, { useState } from 'react';
import { Form, Input, Modal } from 'antd';

const EditUser = ({ data, isModalVisible, closeModal, editUser }) => {
  const [name, setName] = useState(data.name);
  const [email, setEmail] = useState(data.email);
  const [phone, setPhone] = useState(data.phone);
  const [website, setWebsite] = useState(data.website);

  const handleOk = (e) => {
    console.log(name, email, phone, website, e);
    closeModal();
    editUser({
      name,
      email,
      phone,
      website,
      username: data.username,
      id: data.id,
    });
  };

  const handleCancel = () => {
    closeModal();
  };
  return (
    <Modal
      title="Basic Modal"
      visible={isModalVisible}
      onCancel={handleCancel}
      okButtonProps={{
        form: 'myForm',
        key: 'submit',
        htmlType: 'submit',
      }}
      destroyOnClose
    >
      <Form
        id="myForm"
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          name,
          email,
          phone,
          website,
        }}
        onFinish={handleOk}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input onChange={(e) => setName(e.target.value)} />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              type: 'email',
            },
          ]}
        >
          <Input onChange={(e) => setEmail(e.target.value)} />
        </Form.Item>
        <Form.Item
          label="Phone"
          name="phone"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input onChange={(e) => setPhone(e.target.value)} />
        </Form.Item>
        <Form.Item
          label="Website"
          name="website"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Input onChange={(e) => setWebsite(e.target.value)} />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default EditUser;
