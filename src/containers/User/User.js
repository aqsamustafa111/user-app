import React, { useState, useEffect } from 'react';
import UsersList from '../../components/UsersList/UsersList';
import Loading from '../../components/Common/Loading/Loading';
import { user } from '../../actions';

const { fetchUsers } = user;

const User = () => {
  const [usersData, setUsersData] = useState(null);
  const [loading, setLoading] = useState(false);

  const getUsers = async () => {
    const response = await fetchUsers();

    if (response.status === 200) {
      const tempUsersData = response.data.map((data) => {
        const tempUser = {};

        tempUser.id = data.id;
        tempUser.username = data.username;
        tempUser.name = data.name;
        tempUser.email = data.email;
        tempUser.phone = data.phone;
        tempUser.website = data.website;

        return tempUser;
      });

      console.log('users', tempUsersData);
      setUsersData(tempUsersData);
    } else {
      console.log('error', response);
    }
  };

  const deleteUserHandler = (userId) => {
    const tempUsersData = usersData.filter((data) => data.id !== userId);
    setUsersData(tempUsersData);
  };

  const editUserHandler = (editData) => {
    console.log('edit user handler', editData);
    const tempUsersData = usersData.map((data) => {
      if (data.id === editData.id) {
        return editData;
      }
      return data;
    });
    setUsersData(tempUsersData);
  };

  useEffect(() => {
    (async () => {
      setLoading(true);
      await getUsers();
      console.log('done');
      setLoading(false);
    })();
  }, []);

  return (
    <div>
      {!loading ? (
        usersData && (
          <UsersList
            usersData={usersData}
            deleteUser={deleteUserHandler}
            editUser={editUserHandler}
          />
        )
      ) : (
        <Loading fullHeight />
      )}
    </div>
  );
};

export default User;
