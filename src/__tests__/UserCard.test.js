import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { Card } from 'antd';
import { HeartFilled, HeartOutlined } from '@ant-design/icons';
import renderer from 'react-test-renderer';
import users from '../constants/userData';
import UserCard from '../components/UserCard/UserCard';

const deleteUser = jest.fn();
const editUser = jest.fn();

describe('UserCard', () => {
  test('UserCard with first item of usersData-checking title, email, phone, website', () => {
    const tree = renderer.create(
      <UserCard data={users[0]} deleteUser={deleteUser} editUser={editUser} />
    );

    const treeInstance = tree.root;

    const userCard = treeInstance.findByType(Card);

    const [titleEl, emailEl, phoneEl, websiteEl] = userCard.findAllByType(
      Card.Meta
    );

    expect(titleEl.props.title).toBe(users[0].name);
    expect(emailEl.props.description).toBe(users[0].email);
    expect(phoneEl.props.description).toBe(users[0].phone);
    expect(websiteEl.props.description).toBe(users[0].website);

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check the fav button before clicking', async () => {
    const tree = renderer.create(
      <UserCard data={users[0]} deleteUser={deleteUser} editUser={editUser} />
    );

    const treeInstance = tree.root;

    const heartOutlinedIcon = treeInstance.findAllByType(HeartOutlined);
    const heartFilledIcon = treeInstance.findAllByType(HeartFilled);

    expect(heartOutlinedIcon).toBeDefined();
    expect(heartFilledIcon).toEqual([]);

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('check the fav button before and after clicking', async () => {
    const userCard = render(
      <UserCard data={users[0]} deleteUser={deleteUser} editUser={editUser} />
    );

    const heartButton = userCard.getByTestId('fav-btn');

    let heartOutlinedIcon = userCard.queryByTestId('heart-outlined');
    let heartFilledIcon = userCard.queryByTestId('heart-filled');

    expect(heartOutlinedIcon).toBeInTheDocument();
    expect(heartFilledIcon).toBeNull();

    fireEvent.click(heartButton);

    heartOutlinedIcon = userCard.queryByTestId('heart-outlined');
    heartFilledIcon = userCard.queryByTestId('heart-filled');

    expect(heartFilledIcon).toBeInTheDocument();
    expect(heartOutlinedIcon).toBeNull();
  });

  test('check if the delete user button has been called on clicking the delete icon', async () => {
    const userCard = render(
      <UserCard data={users[0]} deleteUser={deleteUser} editUser={editUser} />
    );

    const deleteButton = userCard.getByTestId('delete-btn');

    fireEvent.click(deleteButton);

    expect(deleteUser).toHaveBeenCalled();

    expect(deleteUser).toHaveBeenCalledWith(users[0].id);
  });
});
