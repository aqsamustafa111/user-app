import React from 'react';
import { Card } from 'antd';
import renderer from 'react-test-renderer';
import users from '../constants/userData';
import UsersList from '../components/UsersList/UsersList';

const deleteUser = jest.fn();
const editUser = jest.fn();

describe('UsersList', () => {
  test('UsersList with usersData', () => {
    const tree = renderer.create(
      <UsersList
        usersData={users}
        deleteUser={deleteUser}
        editUser={editUser}
      />
    );

    const treeInstance = tree.root;

    const usersCard = treeInstance.findAllByType(Card);
    usersCard.forEach((userCard, index) => {
      const [titleEl, emailEl, phoneEl, websiteEl] = userCard.findAllByType(
        Card.Meta
      );
      expect(titleEl.props.title).toBe(users[index].name);
      expect(emailEl.props.description).toBe(users[index].email);
      expect(phoneEl.props.description).toBe(users[index].phone);
      expect(websiteEl.props.description).toBe(users[index].website);
    });

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
