import React from 'react';
import renderer from 'react-test-renderer';
import Loading from '../components/Common/Loading/Loading';

describe('Loading', () => {
  test('Loading without any params', () => {
    const tree = renderer.create(<Loading />);

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const img = treeInstance.findByProps({
      className: 'image',
    });
    expect(img.props.src).toBe('loading.gif');

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Loading with fullHeight props', () => {
    const tree = renderer.create(<Loading fullHeight />);

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container fullHeight',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const img = treeInstance.findByProps({
      className: 'image',
    });
    expect(img.props.src).toBe('loading.gif');

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
