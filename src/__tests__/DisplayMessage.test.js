import React from 'react';
import renderer from 'react-test-renderer';
import DisplayMessage from '../components/Common/DisplayMessage/DisplayMessage';

describe('Display Message', () => {
  test('Display Message without any params', () => {
    const tree = renderer.create(<DisplayMessage />);

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const p = treeInstance.findByProps({
      className: 'text',
    });
    expect(p.children[0]).toBeUndefined();

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Display Message with message prop', () => {
    const tree = renderer.create(
      <DisplayMessage message="an unexpected error occurred" />
    );

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const p = treeInstance.findByProps({
      className: 'text',
    });
    expect(p.children[0]).toEqual('an unexpected error occurred');

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Display Message with fullHeight props', () => {
    const tree = renderer.create(<DisplayMessage fullHeight />);

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container fullHeight',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const p = treeInstance.findByProps({
      className: 'text',
    });
    expect(p.children[0]).toBeUndefined();

    expect(tree.toJSON()).toMatchSnapshot();
  });

  test('Display Message with message and fullHeightProp', () => {
    const tree = renderer.create(
      <DisplayMessage message="an unexpected error occurred" fullHeight />
    );

    const treeInstance = tree.root;

    const div = treeInstance.findByProps({
      className: 'container fullHeight',
    });
    expect(treeInstance.children[0]).toEqual(div);

    const p = treeInstance.findByProps({
      className: 'text',
    });
    expect(p.children[0]).toEqual('an unexpected error occurred');

    expect(tree.toJSON()).toMatchSnapshot();
  });
});
