// import { render, screen } from '@testing-library/react';
import userData from '../constants/userData';
import { user } from '../actions';

const { fetchUsers } = user;

describe('actions/user', () => {
  test('fetch all users', async () => {
    const response = await fetchUsers();
    const { data } = response;
    expect(data).not.toBeUndefined();
    expect(data).not.toBeNull();
    expect(data).toEqual(userData);
  });
});
