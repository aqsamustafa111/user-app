import React from 'react';
import User from '../../containers/User/User';

const Home = () => {
  return (
    <div>
      <User />
    </div>
  );
};

export default Home;
