import React, { useState } from 'react';
import { Col, Card, Button } from 'antd';
import 'antd/dist/antd.css';
import {
  MailOutlined,
  PhoneOutlined,
  GlobalOutlined,
  HeartOutlined,
  HeartFilled,
  EditOutlined,
  DeleteFilled,
} from '@ant-design/icons';
import EditUser from '../../containers/EditUser/EditUser';
import styles from './UserCard.module.css';

const UserCard = ({ data, deleteUser, editUser }) => {
  const [isFav, setIsFav] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const favHandler = () => {
    if (isFav) {
      setIsFav(false);
    } else {
      setIsFav(true);
    }
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const closeModal = () => {
    setIsModalVisible(false);
  };

  return (
    <Col xs={24} md={8} xl={6}>
      <Card
        cover={
          <img
            alt={`avatar ${data.username}`}
            src={`https://avatars.dicebear.com/v2/avataaars/${data.username}.svg?options[mood][]=happy`}
            className={styles.avatar}
          />
        }
        className={styles.card}
        actions={[
          <Button
            data-testid="fav-btn"
            size="large"
            type="text"
            icon={
              isFav ? (
                <HeartFilled
                  style={{ color: 'red' }}
                  data-testid="heart-filled"
                />
              ) : (
                <HeartOutlined
                  style={{ color: 'red' }}
                  data-testid="heart-outlined"
                />
              )
            }
            onClick={favHandler}
          />,
          <div className={styles.buttonContainer}>
            <Button
              size="large"
              type="text"
              icon={<EditOutlined />}
              onClick={showModal}
            />
          </div>,
          <div className={styles.buttonContainer}>
            <Button
              data-testid="delete-btn"
              size="large"
              type="text"
              icon={<DeleteFilled />}
              onClick={() => {
                deleteUser(data.id);
              }}
            />
          </div>,
        ]}
        data-testid="user-card"
      >
        <Card.Meta className={styles.cardMeta} title={data.name} />
        <Card.Meta
          className={styles.cardMeta}
          avatar={<MailOutlined />}
          description={data.email}
        />
        <Card.Meta
          className={styles.cardMeta}
          avatar={<PhoneOutlined />}
          description={data.phone}
        />
        <Card.Meta
          className={styles.cardMeta}
          avatar={<GlobalOutlined />}
          description={data.website}
        />
      </Card>

      <EditUser
        data={data}
        isModalVisible={isModalVisible}
        showModal={showModal}
        closeModal={closeModal}
        editUser={editUser}
      />
    </Col>
  );
};

export default UserCard;
