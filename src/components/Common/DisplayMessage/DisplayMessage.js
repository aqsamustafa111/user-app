import React from 'react';
import styles from './DisplayMessage.module.css';

const DisplayMessage = ({ message, fullHeight = false }) => {
  return (
    <div
      className={
        fullHeight
          ? `${styles.container} ${styles.fullHeight}`
          : styles.container
      }
    >
      <p className={styles.text}>{message}</p>
    </div>
  );
};

export default DisplayMessage;
