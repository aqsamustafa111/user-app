import React from 'react';
import LoadingGif from '../../../assets/images/loading.gif';
import styles from './Loading.module.css';

const Loading = ({ fullHeight = false }) => {
  return (
    <div
      className={
        fullHeight
          ? `${styles.container} ${styles.fullHeight}`
          : styles.container
      }
      data-testid="loading"
    >
      <img className={styles.image} src={LoadingGif} alt="loading" />
    </div>
  );
};

export default Loading;
