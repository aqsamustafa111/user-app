import React from 'react';
import { Row } from 'antd';
import 'antd/dist/antd.css';
import UserCard from '../UserCard/UserCard';

const UsersList = ({ usersData, ...restProps }) => {
  const renderUsersData = usersData.map((data) => {
    return <UserCard key={data.id} data={data} {...restProps} />;
  });
  return (
    <div data-testid="users-list">
      <Row gutter={[24, 24]}>{renderUsersData}</Row>
    </div>
  );
};

export default UsersList;
